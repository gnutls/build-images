FROM fedora:41

MAINTAINER Daiki Ueno <ueno@gnu.org>

ARG dnfflags="--allowerasing"

RUN dnf install $dnfflags -y make patch ccache git which autoconf libtool gettext-devel automake autogen \
	autogen-libopts-devel guile22-devel gawk gperf libtasn1-tools bison help2man xz net-tools rsync wget \
	libasan libtsan libasan-static libubsan libubsan-static nodejs datefudge lcov gcovr dieharder \
	openssl socat xz ppp libabigail valgrind libatomic \
	expect softhsm dash iproute opensc \
	gtk-doc texinfo texinfo-tex texlive texlive-supertabular texlive-framed texlive-morefloats texlive-quotchap texlive-eurosym docbook5-style-xsl docbook-style-xsl \
	python3-flake8 python3-jsonschema python3-mypy python3-six \
	python3.6 tox \
	python-unversioned-command zip \
	clang compiler-rt clang-analyzer clang-tools-extra llvm cppcheck \
	fipscheck \
	nmap-ncat tpm-tools trousers swtpm \
	tpm2-tools tpm2-tss-engine-utilities \
	rubygem-rexml \
	beakerlib tmt \
	crypto-policies-scripts meson && \
	for arch in x86_64 i686; do \
		dnf install $dnfflags -y glibc-devel.$arch nettle-devel.$arch \
		libtpms-devel.$arch p11-kit-devel.$arch libidn2-devel.$arch \
		libtasn1-devel.$arch libseccomp-devel.$arch \
		libunistring-devel.$arch openssl-devel.$arch \
		unbound-devel.$arch libcmocka-devel.$arch libev-devel.$arch \
		libkcapi-devel.$arch trousers-devel.$arch tpm2-tss-devel.$arch \
		zlib-ng-devel.$arch brotli-devel.$arch libzstd-devel.$arch \
		pkgconf-pkg-config.$arch; \
	done && \
	dnf install $dnfflags -y 'dnf-command(copr)' && \
	dnf copr -y enable ueno/liboqs && \
	dnf install $dnfflags -y liboqs-devel && \
	dnf clean all

RUN chmod 755 $(find /usr/share/sgml/docbook/xsl-stylesheets-*/ -name dbtoepub -print)
RUN texconfig rehash

RUN wget http://deb.debian.org/debian/pool/main/p/pmccabe/pmccabe_2.8.orig.tar.gz && tar xvf pmccabe_2.8.orig.tar.gz && (cd pmccabe-v2.8 && make && cp pmccabe /usr/local/bin) && rm -rf pmccabe-v2.8 pmccabe_2.8.orig.tar.gz

RUN mkdir -p /usr/local/ && git clone https://gitlab.com/libidn/gnulib-mirror.git /usr/local/gnulib
ENV GNULIB_SRCDIR /usr/local/gnulib
ENV GNULIB_TOOL /usr/local/gnulib/gnulib-tool
