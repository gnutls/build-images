#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

: ${SQUASH=0}
: ${OPTIONS=}

echo "ref: $CI_COMMIT_REF_NAME"
echo "namespace: $CI_PROJECT_NAMESPACE"
echo "project: $CI_PROJECT_NAME"

PODMAN=$(command -v podman)
if test -z "${PODMAN}";then
	PODMAN=$(command -v docker)
else
	test "${SQUASH}" = 1 && OPTIONS="$OPTIONS --squash"
fi

if test -z "${PODMAN}";then
	echo "Could not detect either podman or docker"
	exit 1
fi

master_build=0
if test "$CI_COMMIT_REF_NAME" = "master" && test "$CI_PROJECT_NAMESPACE" = "gnutls" && test "$CI_PROJECT_NAME" = "build-images";then
	master_build=1
fi

set -e
${PODMAN} login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $registry

${PODMAN} build ${OPTIONS} -t $registry/$project:$image $startdir

if test $master_build = 0;then
	echo "Not a master build"
	exit 0
else
	${PODMAN} push $registry/$project:$image
fi

${PODMAN} logout $registry

exit 0
